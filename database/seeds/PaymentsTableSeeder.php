<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Payment;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = 5;
        $payments = [];
        $now = Carbon::now('Europe/Chisinau');
        $userId = DB::table('users')
            ->pluck('id')
            ->first();

        $credits = Payment::CREDITS;

        for ($i = 0; $i < $records; $i++) {
            $payments[] = [
                'recipient_id' => $userId,
                'payer_id'     => $userId,
                'amount'       => $credits[rand(0, count($credits) - 1)],
                'created_at'   => $now,
                'updated_at'   => $now,
            ];
        }

        DB::table('payments')->insert($payments);
    }
}
