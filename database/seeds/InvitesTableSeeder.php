<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Support\Str;

class InvitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = 5;
        $invites = [];
        $now     = Carbon::now('Europe/Chisinau');
        $userId  = DB::table('users')
            ->pluck('id')
            ->first();

        $faker = Faker::create();

        for ($i = 0; $i < $records; $i++) {
            $invites[] = [
                'user_id'    => $userId,
                'code'       => Str::random(),
                'email'      => $faker->unique()->safeEmail,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        DB::table('invites')->insert($invites);
    }
}
