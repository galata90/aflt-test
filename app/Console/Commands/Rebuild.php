<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Rebuild extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:rebuild {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild the application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $confirm = $this->confirm('Are you sure you want to run the service? It will erase all the data you have.');

        if ($confirm) {
            $updateRepos = $this->option('update');
            if ($updateRepos) {
                $this->info('Updating composer dependencies');
                exec("composer update");
                exec("composer dump-autoload -o");
            }
            $this->info('Running the manager...');
            $this->call('clear-compiled');
            $this->call('optimize');
            $this->runMigrationsAndSeeds();
            $this->clearAppCache();
            $this->info('The application has been rebuilt. Enjoy.');
        } else {
            $this->info('Ok. Bye.');
        }
    }

    /**
     * Run migrations and seeds
     *
     * @return void
     */
    private function runMigrationsAndSeeds()
    {
        $this->call('migrate:refresh');
        $this->call('db:seed');
    }

    /**
     * Clear application cache, routes and configs
     *
     * @return void
     */
    private function clearAppCache()
    {
        $this->call('cache:clear');
        $this->call('route:clear');
        $this->call('config:clear');
        $this->call('config:cache');
    }
}
