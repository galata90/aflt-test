<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Invite extends Model
{
    protected $table = 'invites';

    protected $fillable = [
        'user_id',
        'code',
        'email',
        'accepted_at'
    ];

    /**
     * Get invite owner
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Set invite as accepted
     *
     * @return bool
     */
    public function setAccepted()
    {
        $this->accepted_at = Carbon::now('Europe/Chisinau');
        return $this->save();
    }
}