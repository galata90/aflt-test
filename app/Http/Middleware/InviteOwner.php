<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InviteOwner
{
    /**
     * Check if user is invite owner.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $inviteId = $request->route('id');
        $invite = DB::table('invites')
            ->where('id', $inviteId)
            ->first();
        if (!$invite || $invite->user_id != $user->id) {
            abort('404');
        }

        return $next($request);
    }
}
