<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Support\Facades\{Auth, Validator};
use App\User;
use App\Invite;

class RegisterController extends Controller
{
    use RedirectsUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @param string $code
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm($code = null)
    {
        return view('auth.register', ['code' => $code]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($request->has('code')) {
            $isValid = $this->isCodeValid($request->get('code'), $request->get('email'));
            $validator->after(function($validator) use ($isValid) {
                if (!$isValid) {
                    $validator->errors()
                        ->add('code', 'Wrong invite code. Check your invite url and try again.');
                }
            });
        }

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $newUser = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ];

        if (isset($data['code'])) {
            $invite = Invite::where('email', $data['email'])
                ->where('code', $data['code'])
                ->first();
            $invite->setAccepted();
            $newUser['invited_by'] = $invite->user_id;
        }

        return User::create($newUser);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
    }

    /**
     * Check if invite code is valid
     *
     * @param $code
     * @param $email
     * @return bool
     */
    protected function isCodeValid($code, $email)
    {
        if (empty($code) || empty($email)) {
            return false;
        }

        $invite = Invite::where('email', $email)
            ->where('code', $code)
            ->first();

        if (!is_null($invite)) {
            return true;
        }

        return false;
    }
}
