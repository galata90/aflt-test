<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $invitesCount = $user->invites()->count();
        $invitesAccepted = $user->invites()->whereNotNull('accepted_at')->count();

        return view('home')->with([
            'invitesCount' => $invitesCount,
            'invitesAccepted' => $invitesAccepted,
            'balance' => $user->balance()
        ]);
    }
}
