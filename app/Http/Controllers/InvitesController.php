<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\{Auth, Validator};
use Illuminate\Support\Str;
use App\Invite;

class InvitesController extends Controller
{
    protected $invite;

    public function __construct(Invite $invite)
    {
        $this->invite = $invite;
        $this->middleware('invite.owner')->only('destroy');
    }

    public function index()
    {
        $invites = $this->invite
            ->where('user_id', Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return view('invites.index')->with('invites', $invites);
    }

    public function create()
    {
        return view('invites.create');
    }

    public function store(Request $request)
    {
        $userId = Auth::user()->id;
        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                Rule::unique('invites')->where('user_id', $userId)
            ]
        ]);

        if ($validator->passes()) {
            $invite = new Invite();
            $invite->user_id = $userId;
            $invite->code = Str::random();
            $invite->email = $request->get('email');
            $invite->save();

            return response()->json([
                'success' => true,
                'message' => 'Success!'
            ]);
        }

        return response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ]);

    }

    public function destroy($id)
    {
        $invite = $this->invite->find($id);
        if (!is_null($invite)) {
            $invite->delete();
        }
        return redirect()->back();
    }
}
