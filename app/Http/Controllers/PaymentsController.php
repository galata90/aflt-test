<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Validator};
use App\Payment;

class PaymentsController extends Controller
{
    protected $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public function history()
    {
        $payments = $this->payment
            ->with('payer')
            ->where('recipient_id', Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('payments.history')->with([
            'payments' => $payments,
            'credits' => Payment::CREDITS
        ]);
    }

    public function create()
    {
        return view('payments.create');
    }

    public function pay(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'amount' => 'required|integer|in:' . implode(',', Payment::CREDITS)
        ]);

        if ($validator->passes()) {
            $payment = new Payment();
            $payment->recipient_id = $user->id;
            $payment->payer_id = $user->id;
            $payment->amount = $request->get('amount');
            $payment->save();

            if (!is_null($user->inviter)) {
                $payment = new Payment();
                $payment->remunerate($user->inviter->id, $request->get('amount'));
            }

            return response()->json([
                'success' => true,
                'message' => 'Success!'
            ]);
        }

        return response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ]);

    }
}
