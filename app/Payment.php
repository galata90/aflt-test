<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Payment extends Model
{
    /**
     * Available credits
     *
     * @var array
     */
    const CREDITS = [100, 200, 300, 400, 500];

    protected $table = 'payments';

    protected $fillable = [
        'recipient_id',
        'payer_id',
        'amount',
    ];

    /**
     * Get payment recipient
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipient_id', 'id');
    }

    /**
     * Get payment payer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payer()
    {
        return $this->belongsTo(User::class, 'payer_id', 'id');
    }

    /**
     * Remunerate inviter with 10% of amount
     *
     * @param $recipientId
     * @param $sum
     * @return bool
     */
    public function remunerate($recipientId, $sum)
    {
        $this->recipient_id = $recipientId;
        $this->payer_id = Auth::user()->id;
        $this->amount = (int)($sum / 100) * 10;
        return $this->save();
    }
}