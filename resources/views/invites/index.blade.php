@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            Invites
                        </div>
                        <div class="col-md-6">
                            <button type="button"
                                    class="btn btn-xs btn-success pull-right"
                                    data-toggle="modal"
                                    data-target="#inviteCreate">Create</button>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    @if (!$invites->isEmpty())
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Email</th>
                                <th>Code</th>
                                <th>Created</th>
                                <th>Accepted</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($invites as $invite)
                                <tr>
                                    <td>{{ $invite->id }}</td>
                                    <td>{{ $invite->email }}</td>
                                    <td>{{ $invite->code }}</td>
                                    <td>{{ date('d-m-Y H:i', strtotime($invite->created_at)) }}</td>
                                    @if(!empty($invite->accepted_at))
                                        <td>{{ date('d-m-Y H:i', strtotime($invite->accepted_at)) }}</td>
                                    @else
                                        <td>No</td>
                                    @endif
                                    <th>
                                        <a href="{{ route('invites.destroy', ['id' => $invite->id]) }}" class="btn btn-xs btn-danger pull-right">Delete</a>
                                    </th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <h4 style="text-align: center">No invites</h4>
                    @endif
                    {!! $invites->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@include('invites.create')

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('form').submit(function (event) {
                event.preventDefault();

                $('.form-group').removeClass('has-error');
                $('.help-block').remove();

                var formData = {
                    'email': $('input[name=email]').val()
                };

                $.ajax({
                    type: 'POST',
                    url: '{{ route('invites.store') }}',
                    data: formData,
                    dataType: 'json',
                    encode: true
                }).done(function (data) {
                    if (!data.success) {
                        if (data.errors.email) {
                            $('#emailGroup').addClass('has-error');
                            $('#emailGroup .col-md-6').append('<div class="help-block">' + data.errors.email[0] + '</div>');
                        }
                    } else {
                        $('#inviteCreate').modal('hide');
                        location.reload(true);
                    }
                });
            });

            $('#inviteCreate').on('hide.bs.modal', function () {
                $('.form-group').removeClass('has-error');
                $('.help-block').remove();
                $('input[name=email]').val('');
            });
        });
    </script>
@endsection
