@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <p>Created invites: <span class="badge">{{ $invitesCount }}</span></p>
                        </div>
                        <div class="col-md-4">
                            <p>Accepted invites: <span class="badge">{{ $invitesAccepted }}</span></p>
                        </div>
                        <div class="col-md-4">
                            <p>Current balance: <span class="badge">{{ $balance }}</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
