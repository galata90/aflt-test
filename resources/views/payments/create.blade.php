<div class="modal fade" id="paymentCreate" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add credit</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('payments.pay') }}">
                    {{ csrf_field() }}

                    <div id="amountGroup" class="form-group">
                        <label for="amount" class="col-md-4 control-label">Amount</label>

                        <div class="col-md-6">
                            <select id="amount" class="form-control">
                                <option></option>
                                @foreach($credits as $credit)
                                    <option value="{{ $credit }}">{{ $credit }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-sm btn-success">Pay</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>