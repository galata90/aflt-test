@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            Payments
                        </div>
                        <div class="col-md-6">
                            <button type="button"
                                    class="btn btn-xs btn-success pull-right"
                                    data-toggle="modal"
                                    data-target="#paymentCreate">Add</button>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    @if (!$payments->isEmpty())
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Payer</th>
                                <th>Amount</th>
                                <th>Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td>{{ $payment->id }}</td>
                                    <td>{{ $payment->payer->email }}</td>
                                    <td>{{ $payment->amount }}</td>
                                    <td>{{ date('d-m-Y H:i', strtotime($payment->created_at)) }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="4" align="right">Total: {{ Auth::user()->balance() }}</td>
                            </tr>
                        </tbody>
                    </table>
                    @else
                        <h4 style="text-align: center">No payments</h4>
                    @endif
                    {!! $payments->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@include('payments.create')

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('form').submit(function (event) {
                event.preventDefault();

                $('.form-group').removeClass('has-error');
                $('.help-block').remove();

                var formData = {
                    'amount': $('#amount').val()
                };

                $.ajax({
                    type: 'POST',
                    url: '{{ route('payments.pay') }}',
                    data: formData,
                    dataType: 'json',
                    encode: true
                }).done(function (data) {
                    if (!data.success) {
                        if (data.errors.amount) {
                            $('#amountGroup').addClass('has-error');
                            $('#amountGroup .col-md-6').append('<div class="help-block">' + data.errors.amount[0] + '</div>');
                        }
                    } else {
                        $('#paymentCreate').modal('hide');
                        location.reload(true);
                    }
                });
            });

            $('#paymentCreate').on('hide.bs.modal', function () {
                $('.form-group').removeClass('has-error');
                $('.help-block').remove();
                $('#amount').prop('selectedIndex', 0);
            });
        });
    </script>
@endsection
