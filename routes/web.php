<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

Route::get('register/{code?}', 'Auth\RegisterController@showRegistrationForm');
Route::post('register', 'Auth\RegisterController@register');

Route::group(['middleware' => 'auth'], function () {
    Route::get('home', 'HomeController@index');

    //invites routes
    Route::get('invites', [
        'as'   => 'invites.index',
        'uses' => 'InvitesController@index'
    ]);
    Route::get('invites/{id}/destroy', [
        'as'   => 'invites.destroy',
        'uses' => 'InvitesController@destroy'
    ]);
    Route::post('invites', [
        'as'   => 'invites.store',
        'uses' => 'InvitesController@store'
    ]);

    //payments routes
    Route::get('payments', [
        'as'   => 'payments.history',
        'uses' => 'PaymentsController@history'
    ]);
    Route::post('payments', [
        'as'   => 'payments.pay',
        'uses' => 'PaymentsController@pay'
    ]);
});
